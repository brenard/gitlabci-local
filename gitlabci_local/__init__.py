# Components
from gitlabci_local.package.version import Version

# Version
__version__ = Version.get()
